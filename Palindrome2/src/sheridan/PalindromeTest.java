package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid value for palindrome",
				Palindrome.isPalindrome("Anna"));
	}

	@Test
	public void testIsPalindromeNegative(){
		assertFalse("Invalid value for palindrome",
				Palindrome.isPalindrome("AbC"));
	}
	
	@Test
	public void testIsPalindromeBoundaryIn(){
		assertTrue("Invalid value for palindrome",
				Palindrome.isPalindrome("Aa"));
	}
	
	@Test
	public void testIsPalindromeBoundaryOut(){
		assertFalse("Invalid value for palindrome",
				Palindrome.isPalindrome("Racer car"));
	}
	
}
